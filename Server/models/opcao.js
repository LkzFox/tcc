const {sequelize, Sequelize} = require('../config/configuration');

const Opcao = sequelize.define(
	'opcao',
	{
		texto: Sequelize.STRING(100)
	},
	{
		tableName: 'opcao',
		timestamps: false
	}
)

module.exports = Opcao;