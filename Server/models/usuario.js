const {sequelize, Sequelize} = require('../config/configuration');

const Usuario = sequelize.define(
	'usuario',
	{
		nome: Sequelize.STRING(100),
		email: Sequelize.STRING(100),
		senha: Sequelize.STRING(500)
	},
	{
		tableName: 'usuario',
		timestamps: false
	}
)

module.exports = Usuario;