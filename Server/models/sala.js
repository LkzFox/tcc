const {sequelize, Sequelize} = require('../config/configuration');

const Sala = sequelize.define(
	'sala',
	{
		nome: Sequelize.STRING(100),
		status: Sequelize.BOOLEAN,
		tag: Sequelize.STRING(100)
	},
	{
		tableName: 'sala',
	}
)

module.exports = Sala;