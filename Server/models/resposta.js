const {sequelize, Sequelize} = require('../config/configuration');

const Resposta = sequelize.define(
	'resposta',
	{
		pergunta_a: Sequelize.INTEGER,
		pergunta_b: Sequelize.INTEGER,
		pergunta_c: Sequelize.INTEGER,
		pergunta_d: Sequelize.INTEGER,
		pergunta_e: Sequelize.INTEGER,
		pergunta_f: Sequelize.INTEGER,
		pergunta_g: Sequelize.INTEGER,
		pergunta_h: Sequelize.INTEGER,
		pergunta_i: Sequelize.INTEGER,
		pergunta_j: Sequelize.INTEGER,
		pergunta_k: Sequelize.INTEGER,
		pergunta_l: Sequelize.INTEGER,
		pergunta_m: Sequelize.INTEGER,
		pergunta_n: Sequelize.INTEGER,
		finalizado: Sequelize.BOOLEAN
	},
	{
		tableName: 'resposta',
		updatedAt: false
	}
)

module.exports = Resposta;