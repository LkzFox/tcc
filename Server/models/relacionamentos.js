const {sequelize} = require('../config/configuration');

const Modelos = {
	Usuario: require('./usuario'),
	Sala: require('./sala'),
	Resposta: require('./resposta'),
	Pergunta: require('./pergunta'),
	Opcao: require('./opcao')
}

Modelos.Usuario.hasOne(Modelos.Sala);
Modelos.Sala.belongsTo(Modelos.Usuario);

Modelos.Sala.hasOne(Modelos.Resposta);
Modelos.Resposta.belongsTo(Modelos.Sala);

Modelos.Resposta.belongsTo(Modelos.Usuario);

Modelos.Sala.belongsToMany(Modelos.Usuario, {through: 'estudante', timestamps: false});
Modelos.Usuario.belongsToMany(Modelos.Sala, {through: 'estudante', timestamps: false});

Modelos.Opcao.belongsTo(Modelos.Pergunta, {foreignKey: 'perguntaId'});

Modelos.Resposta.belongsToMany(Modelos.Opcao, {through: 'gabarito', timestamps: false, foreignKey: 'respostaId'});
Modelos.Opcao.belongsToMany(Modelos.Resposta, {through: 'gabarito', timestamps: false, foreignKey: 'perguntaId'});

// sequelize.sync();
// sequelize.sync({force: true});

module.exports = Modelos;