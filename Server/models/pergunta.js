const {sequelize, Sequelize} = require('../config/configuration');

const Pergunta = sequelize.define(
	'pergunta',
	{
		texto: Sequelize.STRING(100)
	},
	{
		tableName: 'pergunta',
		timestamps: false
	}
)

module.exports = Pergunta;