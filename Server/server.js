const express = require('express');
const bodyParser = require('body-parser');
const {sequelize} = require('./config/configuration');

app = express();

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({
	extended: true
}));

app.use(function(req, res, next) {
		
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Credentials", "true");
	res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-access-token");
	
	if(req.method !== 'OPTIONS'){
		console.log("");
		console.log("--------------- REQUEST ---------------");
		console.log('Method:', req.method);
		console.log('Url:', req.url);
		console.log("########################################");			
	}
	next();
});


require('./routes/login.js')(app);
require('./routes/resposta.js')(app);
require('./routes/sala.js')(app);
require('./routes/usuario.js')(app);
require('./routes/questionario.js')(app);

require('./models/relacionamentos');

app.listen(3009);

sequelize
  .authenticate()
  .then(() => {
    console.log('Conectado com sucesso na porta 3009.');
  })
  .catch(err => {
    console.error('Erro ao conectar no banco:', err);
  });