const {Usuario} = require('../models/relacionamentos');

function criar(req, res) {
	Usuario.create(req.body)
	.then(result => {
		res.json(result);
	})
	.catch(err => {
		console.log(err);
		res.status(500).send(err);
	})
}

function atualizar(req, res) {
	
}

module.exports = {
	criar: criar,
	atualizar: atualizar
}

