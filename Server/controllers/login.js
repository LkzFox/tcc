const {Usuario} = require('../models/relacionamentos');

function entrar(req, res) {
	Usuario.find(
		{
			where: {
				email: req.body.email,
				senha: req.body.senha
			}
		}
	)
	.then(resposta => {
		res.json({
			conectado: Boolean(resposta),
			usuario: resposta.id
		})
	})
	.catch(err => {
		res.send(err);
	})
}

module.exports = {
	entrar: entrar
}