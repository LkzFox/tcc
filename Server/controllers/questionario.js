const {Pergunta, Opcao} = require('../models/relacionamentos');

function perguntas(req, res) {
	Pergunta.findAll()
	.then(result => {
		res.json(result);
	})
	.catch(err => {
		res.status(500).send(err)
	})
}

function opcoes(req, res) {
	Opcao.findAll({
		where: {
			perguntaId: req.params.id
		}
	})
	.then(result => {
		res.json(result)
	})
	.catch(err => {
		res.status(500).send(err);
	})
}

module.exports = {
	perguntas: perguntas,
	opcoes: opcoes
}