const {Sala} = require('../models/relacionamentos');
const {sequelize} = require('../config/configuration');

function criar(req, res) {
	Sala.create(req.body)
	.then(result => {
		res.json(result);
	})
	.catch(err => {
		res.status(500).send(err);
	})
}

function atualizar(req, res) {
	
}

function entrar(req, res) {
	Sala.findOne({
		where: {tag: req.params.id}
	})
	.then(result => {
		if(result.status){
			sequelize.query(`INSERT INTO estudante ("salaId", "usuarioId") values (${result.id}, ${req.body.id})`)
			.then( inserido => {
				res.json({resultado: true, mensage: 'Cadastrado na sala', aluno: inserido, salaId: result.id});
			})
			.catch(err => {
				res.json({resultado: true, mensage: 'Ja Cadastrado', salaId: result.id});
			})
		}else{
			res.json({resultado: false, mensage: 'Sala fechada'});
		}
	})
	.catch(err => {
		console.log(err);
		res.status(500).send(err);
	})
}


module.exports = {
	criar: criar,
	atualizar: atualizar,
	entrar: entrar
}