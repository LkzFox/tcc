const {Resposta} = require('../models/relacionamentos');
const {sequelize, Sequelize} = require('../config/configuration');

function atualizar(req, res) {

}

function pegar(req, res) {
	
}

function responder(req, res) {
	Resposta.create(montarResposta(req.body.respostas, req.body.salaId, req.body.usuarioId))
	.then(result => {
		res.json(result);
	})
	.catch(err => {
		console.log(err);
		res.status(500).send()
	})
}

function montarResposta(vetor, sala, usu) {
	const obj = {};
	obj.salaId = sala;
	obj.usuarioId = usu;

	vetor.forEach( (ele, ind) => {
		obj[ele.campo] = ele.id
	})
	
	return obj
}

module.exports = {
	atualizar: atualizar,
	pegar: pegar,
	responder: responder
}