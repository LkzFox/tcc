module.exports = function (app) {
	
	const controle = require('../controllers/sala');

	app.route('/sala')
	.post(controle.criar)
	.put(controle.atualizar)

	app.route('/sala/entrar/:id')
	.post(controle.entrar);
}