module.exports = function (app) {
	
	const controle = require('../controllers/resposta');

	app.route('/resposta')
	.post(controle.responder)
	.put(controle.atualizar)

	app.route('/resposta/:id')
	.get(controle.pegar)
}