module.exports = function (app) {
	
	const controle = require('../controllers/usuario');

	app.route('/usuario')
	.post(controle.criar)
	.put(controle.atualizar)

}