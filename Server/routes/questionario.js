module.exports = function (app) {
	
	const controle = require('../controllers/questionario');

	app.route('/questionario')
	.get(controle.perguntas)

	app.route('/opcoes/:id')
	.get(controle.opcoes);
}