
module.exports = class Funcoes {	

//	Calcula a distância euclidiana entre dois vetores (entrada e neurônio)
	static Distancia(arr1, arr2) {
		let temp = 0;
		
		for(var i = 0; i < arr1.length; i++ ) {
			temp += Math.pow((arr1[i] - arr2[i]) , 2);
		}
		
		return Math.sqrt(temp);
	}
	
//	Encontra o neurônio com melhor classificação
	static PegarBMU(lista, entrada){
		let distanciaTemp, melhor = Number.MAX_VALUE;
		let index = 0;
		
		for(var i = 0; i < lista.length; i++) {
			distanciaTemp = Funcoes.Distancia(lista[i].pesos, entrada);
//			System.out.println(distanciaTemp);
			if(distanciaTemp < melhor) {
				melhor = distanciaTemp;
				index = i;
			}
		}
		
		return index;
	}
	
}