// const config = require('./configuracao');

module.exports = class Neuronio {

	constructor(qtPesos = 3, pesos = []){
		this._quantidadePesos = qtPesos;
		this._pesos = pesos;
		if(pesos.length === 0){

			for(var i = 0; i < this._quantidadePesos; i++){
				this._pesos.push(parseFloat(Math.random().toFixed(2)))
			}
			
		}
	}

	get pesos(){
		return this._pesos;
	}

	set pesos(nPesos){
		this._pesos = nPesos;
	}

	get quantidadePesos(){
		return this._quantidadePesos;
	}

	set quantidadePesos(nQt){
		this._quantidadePesos = nQt;
	}
}

// let neu = new Neuronio();

// console.log(neu);
// console.log(neu.pesos);
// console.log(neu.quantidadePesos);