const Funcoes = require('./Funcoes')
const Neuronio = require('./neuronio')

const ArquivoTreino = require('./treino')

class KMap {
	
	constructor() {
		this._quantidadeNeuronios = 8;
		this._maximoInteracoes = 40;
		this._quantidadeVizinhos = 2;
		this._taxaVizinhos = 12;
		this._apredizagem = 0.6;
		this._taxaAprendizagem = 0.8;
		this._neuronios = [];
		
	}
	
	Treinar() {
		for(var interacao = 0; interacao < this._maximoInteracoes; interacao++) {
			// ArrayList<double[]> irisData = Functions.ReadCSVFileIris();

			if(interacao % (this.maximoInteracoes / 5) == 0 || interacao == this.maximoInteracoes - 1) {
				//this.dataChart.put(GenerateDataSetXY(this.clusters, interacao));
			}
			
			// int dados = irisData.size();
			for(var i = 0; i < dados.length; i++) {
				let indexRandom = new Math.round(Math.random() * dados.length)
				let dado = dados.splice(indexRandom, 1);

				let BMU = Functions.PegarBMU(this.neuronios, dado);
				AtualizarVizinhos(BMU, interacao, dado);
			}
			
			if(1 + interacao % this._taxaVizinhos == 0) {
				this._quantidadeVizinhos --;
			}
		}
	}

	AtualizarVizinhos(BMU, interacao, entrada) {

		AtualizarPesosNeuronio(BMU, interacao, entrada);
		
		for(var i = 1; i <= this.neighborhoodCount; i++) {
			let esquerda = BMU - i;
			let direita = BMU + i;
	
			if(esquerda >= 0) {
				AtualizarPesosNeuronio(esquerda, interacao + 3, entrada);
			}
			
			if(direita < this.clustersAmount) {
				AtualizarPesosNeuronio(direita, interacao + 3, entrada);
			}
		}
	}
	
	AtualizarPesosNeuronio(neuronioIndex, interacao, entrada) {
		novosPesos = [];
		velhosPesos = this._neuronios[neuronioIndex].pesos();
		for(var i = 0; i < this._neuronios[neuronioIndex].quantidadePesos(); i++) {
			novosPesos[i] = velhosPesos[i] + taxaAprendizagem(interacao) * (entrada[i] - velhosPesos[i]);
		}
		this._neuronios[neuronioIndex].pesos = novosPesos;

	}
	
	taxaAprendizagem(interacao) {
		let valor = this._taxaAprendizagem * Math.pow(this._taxaAprendizagem, interacao);
		valor = valor * 1000;
		valor = Math.round(valor);
		valor = valor / 1000;
		return valor;
	}
	
	Classificar(item) {
		let i = 1;
		let quantidade = [];
		
		// for(double[] item : Functions.ReadCSVFileIris()) {
		// 	int cluster = Functions.FindBMU(this.clusters, item);
		// 	quantidade[cluster] = quantidade[cluster] + 1; 
		// 	System.out.println(i + " " + cluster);
		// 	i++;
		// }
		// System.out.println(Arrays.toString(quantidade));

		console.log(Funcoes.PegarBMU(this._neuronios, item));
	}

	
	
}


console.log(new KMap())
console.log(Funcoes.Distancia([0, 0], [5, 5]))

let lista = [
	new Neuronio(2, [1, 1]),
	new Neuronio(2, [7, 7]),
	new Neuronio(2, [10, 1]),
]

console.log(Funcoes.PegarBMU(lista, [6, 2]))