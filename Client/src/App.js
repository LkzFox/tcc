import React, { Component } from 'react';


class App extends Component {

	handleSubmit = e => {
		e.preventDefault();

		fetch("http://localhost:3009/login", {method: 'POST', body: JSON.stringify({email: 'jlkzfp@gmail.com', senha: '123456'}), headers: new Headers({
            'Content-type': 'application/json'})
	})
		.then(res => {
			if (res.ok) {
				return res.json()
			}else{
				throw new Error('Falha no login')
			}
		})
		.then(resposta => {
			console.log(resposta);
		})

		console.log("Submit");
	}

	render() {
		return (
			<div className="loginForm">
				<div className="container">
					<form className="col s12">
						<div className="row">
							<div className="input-field col offset-s3 s6">
								<input placeholder="Email" id="email" type="email" autoFocus={true}/>
								<label htmlFor="email">Email</label>
							</div>
						</div>
						<div className="row">
							<div className="input-field col offset-s3 s6">
								<input placeholder="Senha" id="senha" type="password"/>
								<label htmlFor="senha">Senha</label>
							</div>
						</div>
						<div className="row">
							<div className="input-field col offset-s3 s6">
								<button 
									className="btn waves-effect waves-light" 
									type="submit" 
									name="action" 
									onClick={this.handleSubmit}>
									Entrar
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		);
	}
}

export default App;
