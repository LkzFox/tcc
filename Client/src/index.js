import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware, combineReducers} from 'redux';
import thunkMiddleware from 'redux-thunk';
import registerServiceWorker from './registerServiceWorker';

import FormUsuario from './paginas/registro-usuario';
import FormLogin from './paginas/login';
import FormSala from './paginas/registro-sala';
import Inicio from './paginas/inicio';
import Questionario from './paginas/questionario';

import ReducerRegistro from './reducers/registro';
import ReducerLogin from './reducers/login';
import ReducerQuestionario from './reducers/questionario';
import ReducerSala from './reducers/sala';

const reducers = combineReducers({ReducerRegistro, ReducerLogin, ReducerQuestionario, ReducerSala});
const store = createStore(reducers, applyMiddleware(thunkMiddleware));


ReactDOM.render(
	<Provider store={store}>
		<Router>
			<Switch>
				<Route exact path="/" component={FormLogin} />
				<Route exact path="/cadastrar" component={FormUsuario} />
				<Route exact path="/inicio" component={Inicio} />
				<Route exact path="/cadastro/sala" component={FormSala} />
				<Route exact path="/questionario" component={Questionario} />
			</Switch>
		</Router>
	</Provider>
	, document.getElementById('root'));
registerServiceWorker();
