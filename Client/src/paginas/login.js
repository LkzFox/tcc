import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import {connect} from 'react-redux';

import UsuarioAPI from '../apis/UsuarioAPI';

class Login extends Component{

	handleSubmit = event => {
		event.preventDefault();
		const usuario = {
			senha: this._senha.value,
			email: this._email.value
		}
		this.props.entrar(usuario);
		console.log("Enviado", usuario)
	}

	render(){
		if (this.props.logado){
			return <Redirect push to="/inicio"/>
		}
		return(
			<div className="loginForm">
				<div className="container">
					<form className="col s12">
						<div className="row">
							<div className="input-field col offset-s3 s6">
								<input placeholder="Email" id="email" type="email" autoFocus={true} ref={email => this._email = email}/>
								<label htmlFor="email">Email</label>
							</div>
						</div>
						<div className="row">
							<div className="input-field col offset-s3 s6">
								<input placeholder="Senha" id="senha" type="password" ref={senha => this._senha = senha}/>
								<label htmlFor="senha">Senha</label>
							</div>
						</div>
						<div className="row">
							<div className="input-field col offset-s3 s6">
								<button 
									className="btn waves-effect waves-light" 
									type="submit" 
									name="action" 
									onClick={this.handleSubmit}>
									Entrar
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		)
	}
}

const MapStateToProps = state => {
	return {
		logado: state.ReducerLogin.acesso
	}
}

const MapDispatchToProps = dispatch => {
	return {
		entrar: usuario => {
			dispatch(UsuarioAPI.entrar(usuario));
		}
	}
}

export default connect(MapStateToProps, MapDispatchToProps)(Login);