import React, {Component} from 'react';
// import {Redirect} from 'react-router-dom';
import {connect} from 'react-redux';

import UsuarioAPI from '../apis/UsuarioAPI';

class FormUsuario extends Component{

	handleSubmit = event => {
		event.preventDefault();
		const usuario = {
			nome: this._nome.value,
			email: this._email.value,
			senha: this._senha.value
		}
		this.props.cadastrar(usuario);
	}

	render(){
		return(
			<div className="loginForm">
				<div className="container">
					<form className="col s12">
						<div className="row">
							<div className="input-field col offset-s3 s6">
								<input placeholder="Nome" id="nome" type="text" autoFocus={true} ref={nome => this._nome = nome}/>
								<label htmlFor="nome">Nome</label>
							</div>
						</div>
						<div className="row">
							<div className="input-field col offset-s3 s6">
								<input placeholder="Email" id="email" type="email" ref={email => this._email = email}/>
								<label htmlFor="email">Email</label>
							</div>
						</div>
						<div className="row">
							<div className="input-field col offset-s3 s6">
								<input placeholder="Senha" id="senha" type="password" ref={senha => this._senha = senha}/>
								<label htmlFor="senha">Senha</label>
							</div>
						</div>
						<div className="row">
							<div className="input-field col offset-s3 s6">
								<button 
									className="btn waves-effect waves-light" 
									type="submit" 
									name="action" 
									onClick={this.handleSubmit}>
									Cadastrar
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		)
	}
}

const MapStateToProps = state => {
	return {

	}
}

const MapDispatchToProps = dispatch => {
	return {
		cadastrar: function (usuario) {
			dispatch(UsuarioAPI.cadastrar(usuario));
		}
	}
}

export default connect(MapStateToProps, MapDispatchToProps)(FormUsuario);