import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';

import SalaAPI from '../apis/SalaAPI';
import UsuarioLocal from '../apis/UsuarioLocal';

class Inicio extends Component{

	handleSubmit = event => {
		event.preventDefault();
		this.props.entrar(this._sala.value, {id: UsuarioLocal().usuarioId})
	}

	render(){
		if (this.props.acesso) {
			return <Redirect push to="/questionario" />
		}
		return(
			<div className="loginForm">
				<div className="container">
					<form className="col s12">
						<div className="row">
							<div className="input-field col offset-s3 s6">
								<input placeholder="Ex.: A48YN" id="sala" type="text" autoFocus={true} ref={sala => this._sala = sala} defaultValue="JH87TE2V"/>
								<label htmlFor="sala">Sala</label>
							</div>
						</div>
						<div className="row">
							<div className="input-field col offset-s3 s6">
								<button 
									className="btn waves-effect waves-light" 
									type="submit" 
									name="action" 
									onClick={this.handleSubmit}>
									Entrar
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		)
	}
}

const MapStateToProps = state => {
	return {
		acesso: state.ReducerSala.acesso
	}
}

const MapDispatchToProps = dispatch => {
	return {
		entrar: (id, usuario) => {
			dispatch(SalaAPI.entrar(id, usuario));
		}
	}
}

export default connect(MapStateToProps, MapDispatchToProps)(Inicio);