import React, {Component} from 'react';
import {connect} from 'react-redux';

import SalaAPI from '../apis/SalaAPI';

class FormSala extends Component{

	handleSubmit = event => {
		event.preventDefault();
		const tag = new Date().getTime().toString(36).toUpperCase();
		const sala = {
			nome: this._nome.value,
			status: this._status.checked,
			tag
		}
		this.props.cadastrar(sala);
	}

	render(){
		return(
			<div className="loginForm">
				<div className="container">
					<form className="col s12">
						<div className="row">
							<div className="input-field col offset-s3 s6">
								<input placeholder="Nome" id="nome" type="text" autoFocus={true} ref={nome => this._nome = nome}/>
								<label htmlFor="nome">Nome</label>
							</div>
						</div>
						<div className="row">
							<div className="switch input-field col offset-s3 s6">
							    <label>
							      Fechada
							      <input type="checkbox" ref={status => this._status = status}/>
							      <span className="lever"></span>
							      Aberta
							    </label>
						  	</div>
						</div>
						
						<div className="row">
							<div className="input-field col offset-s3 s6">
								<button 
									className="btn waves-effect waves-light" 
									type="submit" 
									name="action" 
									onClick={this.handleSubmit}>
									Cadastrar
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		)
	}
}

const MapStateToProps = state => {
	return {

	}
}

const MapDispatchToProps = dispatch => {
	return {
		cadastrar: function (sala) {
			dispatch(SalaAPI.cadastrar(sala));
		}
	}
}

export default connect(MapStateToProps, MapDispatchToProps)(FormSala);