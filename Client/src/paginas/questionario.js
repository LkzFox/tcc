import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';

import QuestionarioAPI from '../apis/QuestionarioAPI';
import UsuarioLocal from '../apis/UsuarioLocal';

const campos = ['pergunta_a', 'pergunta_b', 'pergunta_c', 'pergunta_d', 'pergunta_e', 'pergunta_f', 'pergunta_g', 'pergunta_h', 'pergunta_i', 'pergunta_j', 'pergunta_k', 'pergunta_l', 'pergunta_m', 'pergunta_n']

function Pergunta(arg) {
	return(
		<h3>{arg.texto}</h3>
	)
}

function Opcao(arg, icon) {
		let icone;
		if (icon === 1){
    		icone = <i className="material-icons">looks_one</i>
    	} else if(icon === 2){
    		icone = <i className="material-icons">looks_two</i>
    	} else{
    		icone = <i className="material-icons">looks_3</i>
    	}
	return(
		<div className="chip valign-wrapper" style={{display: 'flex', height: 'auto'}} key={arg.id} onClick={event => this.handleAnswer(arg)}>
			{icone}
	    	&nbsp;
	    	<span className="resposta">{arg.texto}</span>
		</div>
	)
}

class Inicio extends Component{

	constructor(props){
		super(props);
		this.state = {
			perguntaAtual: 0,
		}
		this.Opcao = Opcao.bind(this);
		this.respostas = [];
	}

	componentDidMount(){
		this.props.getQuestionario();
	}

	componentWillUpdate(nextProps, nextState){
		if(this.props.questionario.length === 0 && nextProps.questionario.length > 0){
			this.props.getOpcoes(nextProps.questionario[0].id);
		}

		if(this.state.perguntaAtual !== nextState.perguntaAtual && this.props.questionario.length > nextState.perguntaAtual){
			this.props.getOpcoes(nextProps.questionario[nextState.perguntaAtual].id);
		}
	}

	handleSubmit = event => {
		event.preventDefault();
		this.props.entrar(this._sala.value, {id: 5})
	}

	handleAnswer = item => {
		if (this.state.perguntaAtual < this.props.questionario.length) {
			this.respostas.push(item);
			this.setState({
				perguntaAtual: this.state.perguntaAtual + 1
			})
			if (this.state.perguntaAtual === this.props.questionario.length - 1) {
				this.setState({
					finalizado: true
				})
			}
		}
		console.log(this.respostas);
	}

	handleFinish = event => {
		event.preventDefault();
		for(var i = 0; i < this.respostas.length; i++){
			this.respostas[i].campo = campos[i];
		}
		this.props.enviarRespostas(this.respostas, UsuarioLocal().usuarioId, UsuarioLocal().salaId)
	}


	render(){
		if (this.state.finalizado){
			return 	(
				<div className="loginForm">
					<div className="container">
						<form className="col s12 center">
							<input type="submit" className="btn btn-primary" onClick={this.handleFinish} value="Finalizar" />
						</form>
					</div>
				</div>
			)
		}
		return(
			<div className="loginForm">
				<div className="container">
					<form className="col s12 center">
						{this.props.questionario.length > 0 ? 
							Pergunta(this.props.questionario[this.state.perguntaAtual] || {texto: 'Fim'}) : ''
						}
						<div className="row">
							<div className="input-field col offset-s2 s8">
								{this.props.opcoes.length > 0 ?
									<React.Fragment>
								 		{this.Opcao(this.props.opcoes[0], 1)}
								 		{this.Opcao(this.props.opcoes[1], 2)}
								 		{this.Opcao(this.props.opcoes[2], 3)}
									</React.Fragment> : ''
								}
							</div>
						</div>
					</form>
				</div>
			</div>
		)
	}
}

const MapStateToProps = state => {
	return {
		questionario: state.ReducerQuestionario.perguntas,
		opcoes: state.ReducerQuestionario.opcoes
	}
}

const MapDispatchToProps = dispatch => {
	return {
		entrar: (id, usuario) => {
			// dispatch(SalaAPI.entrar(id, usuario));
		},
		getQuestionario: () => {
			dispatch(QuestionarioAPI.perguntas());
		},
		getOpcoes: (id) => {
			dispatch(QuestionarioAPI.opcoes(id));
		},
		enviarRespostas: (respostas, usuarioId, salaId) => {
			dispatch(QuestionarioAPI.enviarRespostas(respostas, usuarioId, salaId))
		}
	}
}

export default connect(MapStateToProps, MapDispatchToProps)(Inicio);

// http://lelivros.love/book/baixar-livro-ja-entendi-gladys-mariotto-em-pdf-epub-e-mobi-ou-ler-online/