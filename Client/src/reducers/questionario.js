const EstadoInicial = {
	perguntas: [],
	opcoes: []
}

export default function (state = EstadoInicial, action) {

	const NovoEstado = Object.assign({}, state);

	if (action.type === "QUESTIONARIO") {
		NovoEstado.perguntas = action.result;
		return NovoEstado
	}

	if (action.type === "OPCOES") {
		NovoEstado.opcoes = action.result;
		return NovoEstado
	}

	return state;
}