const EstadoInicial = {
	acesso: null
}

export default function (state = EstadoInicial, action) {

	const NovoEstado = Object.assign({}, state);

	if (action.type === "SALA-ENTRAR") {
		NovoEstado.acesso = action.result.resultado;
		return NovoEstado
	}

	return state;
}