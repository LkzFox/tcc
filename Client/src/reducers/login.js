const EstadoInicial = {
	acesso: null
}

export default function (state = EstadoInicial, action) {

	const NovoEstado = Object.assign({}, state);

	if (action.type === "LOGIN") {
		NovoEstado.acesso = action.resposta.conectado;
		return NovoEstado
	}

	return state;
}