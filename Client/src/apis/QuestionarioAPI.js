import {questionarioURL, opcoesURL, respostasURL} from './URLS.js';
import FetchObj from './FetchObj';

export default class QuestionarioAPI{

	static perguntas(){
		return dispatch => {
			fetch(questionarioURL, FetchObj('GET'))
			.then(res => {
				if (res.ok) {
					return res.json();
				}else{
					throw new Error('Falha ao obter questionario');
				}
			})
			.then(result => {
				console.log(result);
				dispatch({type: "QUESTIONARIO", result})
			})
		}
	}

	static opcoes(id){
		return dispatch => {
			fetch(opcoesURL(id), FetchObj('GET'))
			.then(res => {
				if (res.ok) {
					return res.json();
				}else{
					throw new Error('Falha ao obter opcoes');
				}
			})
			.then(result => {
				console.log(result);
				dispatch({type: "OPCOES", result})
			})
		}
	}

	static enviarRespostas(respostas, usuarioId, salaId){
		return dispatch => {
			fetch(respostasURL, FetchObj("POST", {respostas, usuarioId, salaId}))
			.then(res => {
				if (res.ok) {
					return res.json()
				}else{
					throw new Error('Falaha ao enviar as respostas')
				}
			})
			.then(result => {
				localStorage.removeItem("tcc-s")
				dispatch({type: "RESPOSTAS", result})
			})
		}
	}


}