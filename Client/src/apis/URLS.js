const base = "http://localhost:3009/";

/* CADASTRO */
export const cadastrarURL = base + 'usuario';
export const loginURL = base + 'login';

/* SALAS */
export const cadastrarSalaURL = base + 'sala';
export const entrarURL = id => `${base}sala/entrar/${id}`;

/* QUESTIONARIO */
export const questionarioURL = base + 'questionario';
export const opcoesURL = id => base + 'opcoes/' + id;
export const respostasURL = base + 'resposta';