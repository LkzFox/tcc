export default function(tipo, corpo) {
	return {
				method: tipo,
				body: JSON.stringify(corpo),
				headers: new Headers({
					'Content-type': 'application/json'
				})
			}
}