import {entrarURL, cadastrarSalaURL} from './URLS.js';
import FetchObj from './FetchObj';

export default class SalaAPI{

	static entrar(id, usuario){
		return dispatch => {
			fetch(entrarURL(id), FetchObj('POST', usuario))
			.then(res => {
				if (res.ok) {
					return res.json();
				}else{
					throw new Error('Falha ao entrar na sala');
				}
			})
			.then(result => {
				localStorage.setItem("tcc-s", result.salaId);
				console.log(result);
				dispatch({type: "SALA-ENTRAR", result})
			})
		}
	}

	static cadastrar(sala){
		return dispatch => {
			fetch(cadastrarSalaURL, FetchObj('POST', sala))
			.then(res => {
				if (res.ok) {
					return res.json();
				}else{
					throw new Error('Erro ao cadastrar sala')
				}
			})
			.then(result => {
				console.log(result);
				dispatch({type: 'SALA', result})
			})
		}
	}
}