import {cadastrarURL, loginURL} from './URLS';
import FetchObj from './FetchObj';

export default class UsuarioAPI{

	static cadastrar(usuario){
		return dispatch => {
			fetch(cadastrarURL, FetchObj("POST", usuario))
			.then(res => {
				if (res.ok) {
					return res.json()
				}else{
					throw new Error('Falha no cadastro')
				}
			})
			.then(resposta => {
				console.log(resposta)
				dispatch({type: "REGISTRO", resposta})
			})
		}
	}

	static entrar(usuario){
		return dispatch => {
			fetch(loginURL, FetchObj("POST", usuario))
			.then(res => {
				if (res.ok) {
					return res.json()
				}else{
					throw new Error('Falha no cadastro')
				}
			})
			.then(resposta => {
				localStorage.setItem("tcc-u", resposta.usuario)
				console.log({type: "LOGIN", resposta})
				dispatch({type: "LOGIN", resposta})
			})
		}
	}
}